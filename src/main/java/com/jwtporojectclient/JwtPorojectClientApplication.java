package com.jwtporojectclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtPorojectClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(JwtPorojectClientApplication.class, args);
    }

}
