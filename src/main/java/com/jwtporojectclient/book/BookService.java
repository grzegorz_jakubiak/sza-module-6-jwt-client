package com.jwtporojectclient.book;

public interface BookService {

    void addBook(String bookToAdd);

    void getBooks();
}
