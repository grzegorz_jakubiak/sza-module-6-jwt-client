package com.jwtporojectclient.book;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.jwtporojectclient.util.KeyUtil;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;

@Component
public class BookJwtGenerator {

    private final static String PATH_TO_PUBLIC_KEY = "src/main/resources/keys/public_key.pem";
    private final static String PATH_TO_PRIVATE_KEY = "src/main/resources/keys/priv.pkcs8";

    public String generateJWT(HashMap<String, String> claims) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        RSAPublicKey publicKey = KeyUtil.getPublicKey(new File(PATH_TO_PUBLIC_KEY));
        RSAPrivateKey privateKey = KeyUtil.getPrivateKey(new File(PATH_TO_PRIVATE_KEY));
        JWTCreator.Builder builder = JWT.create();
        claims.forEach(builder::withClaim);
        return builder.sign(Algorithm.RSA256(publicKey, privateKey));
    }

    public String generateJWT() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        RSAPublicKey publicKey = KeyUtil.getPublicKey(new File(PATH_TO_PUBLIC_KEY));
        RSAPrivateKey privateKey = KeyUtil.getPrivateKey(new File(PATH_TO_PRIVATE_KEY));
        JWTCreator.Builder builder = JWT.create();
        builder.withClaim("name", "Admin");
        builder.withClaim("isAdmin", true);
        return builder.sign(Algorithm.RSA256(publicKey, privateKey));
    }
}
