package com.jwtporojectclient.book;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

@Service
@AllArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookJwtGenerator bookJwtGenerator;

    @Override
    public void addBook(String bookToAdd) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        try {
            headers.add("Authorization", "Bearer " + bookJwtGenerator.generateJWT());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        headers.add("Content-Type","application/json");
        HttpEntity httpEntity = new HttpEntity(bookToAdd, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange("http://localhost:8090/api/books",
                HttpMethod.POST,
                httpEntity,
                String.class);
        System.out.println("status: "+responseEntity.getStatusCodeValue());
    }

    @Override
    public void getBooks() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        try {
            headers.add("Authorization", "Bearer " + bookJwtGenerator.generateJWT());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        headers.add("Content-Type","application/json");
        HttpEntity httpEntity = new HttpEntity(headers);

        RestTemplate restTemplate = new RestTemplate();

        String[] books = restTemplate.exchange("http://localhost:8090/api/books",
                HttpMethod.GET,
                httpEntity,
                String[].class).getBody();

        Arrays.stream(books).forEach(x->System.out.println(x));

    }
}
