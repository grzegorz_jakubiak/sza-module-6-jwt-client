package com.jwtporojectclient.book;

import lombok.AllArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@AllArgsConstructor
public class ClientBook {

    private final BookService bookService;

    @EventListener(ApplicationReadyEvent.class)
    private void processBooks(){
        bookService.getBooks();
        bookService.addBook("Book_"+new Date().getTime());
        bookService.getBooks();
    }


}
